﻿
Public Class MinyanimModels
    Public Property Minyanlist As List(Of BlueBasis.Ecom.Minyanim)
    Public Property ShulListing As List(Of BlueBasis.Ecom.ShulInfo)
    Public Property PreviousThree As List(Of BlueBasis.Ecom.Minyanim)
    Public Property NextThree As List(Of BlueBasis.Ecom.Minyanim)
    Public Property ShulList As List(Of String)
    Public Property ShulName As BlueBasis.Ecom.ShulInfo
    Public Property CurrentShul As String
End Class
