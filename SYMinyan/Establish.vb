﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.IO

Namespace BlueBasis
    Public Class Establish
        Public Shared ReadOnly Property ConnString() As String
            Get
                Return ConfigurationManager.ConnectionStrings("Conn").ConnectionString
            End Get
        End Property

        Public Shared ReadOnly Property Conn() As SqlConnection
            Get
                Dim _Conn As SqlConnection = Nothing

                If HttpContext.Current IsNot Nothing Then
                    If Not HttpContext.Current.Items.Contains("conn") Then
                        HttpContext.Current.Items("conn") = CreateConnection()
                    End If

                    _Conn = HttpContext.Current.Items("conn")
                End If

                Return _Conn
            End Get
        End Property

        Public Shared Function CreateConnection() As SqlConnection
            Return New SqlConnection(ConnString)
        End Function

        Public Shared Property Db() As Database.BlueBasisDbContext
            Get
                Dim _Db As Database.BlueBasisDbContext = Nothing

                If HttpContext.Current IsNot Nothing Then
                    If Not HttpContext.Current.Items.Contains("DbContext") OrElse HttpContext.Current.Items("DbContext") Is Nothing Then
                        HttpContext.Current.Items("DbContext") = CreateDb()
                    End If

                    _Db = HttpContext.Current.Items("DbContext")
                End If

                Return _Db
            End Get
            Set(value As Database.BlueBasisDbContext)
                HttpContext.Current.Items("DbContext") = value
            End Set
        End Property

        Public Shared ReadOnly Property PageName() As String
            Get
                Return Path.GetFileName(HttpContext.Current.Request.PhysicalPath)
            End Get
        End Property

        Public Shared Function CreateDb() As Database.BlueBasisDbContext
            Return New Database.BlueBasisDbContext()
        End Function
    End Class
End Namespace