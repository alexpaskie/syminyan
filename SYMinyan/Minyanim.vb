﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Entity.ModelConfiguration

Namespace BlueBasis.Ecom

#Region "Database Map"
    Partial Public Class MinyanimMap
        Inherits BaseMinyanimMap

    End Class

    Public Class BaseMinyanimMap
        Inherits EntityTypeConfiguration(Of Minyanim)

        Sub New()
            MyBase.New()


            HasKey(Function(m) m.RecID)

            [Property](Function(m) m.RecID).HasColumnName("rec_id")
            [Property](Function(m) m.MLocation).HasColumnName("m_location")
            [Property](Function(m) m.MTime).HasColumnName("m_time")
            [Property](Function(m) m.MDay).HasColumnName("m_day")
            [Property](Function(m) m.MType).HasColumnName("m_type")

            ToTable("minyanim")
        End Sub

    End Class

#End Region

    Partial Public Class Minyanim
        Inherits BaseMinyanim

    End Class

    Public Class BaseMinyanim

        Public Property RecID As Integer
        Public Property MLocation As String
        Public Property MTime As TimeSpan
        Public Property MDay As Integer
        Public Property MType As String
    End Class

End Namespace
