﻿Imports System.Data.Entity

Namespace BlueBasis.Database

    ''' <summary>
    ''' Defines BlueBasis DB tables
    ''' </summary>
    ''' <remarks></remarks>
    Partial Public Class BlueBasisDbContext
        Inherits DbContext

        Public Sub New()
            Me.New(Establish.ConnString)
        End Sub

        Public Sub New(nameOrConnectionString As String)
            MyBase.New(nameOrConnectionString)
        End Sub

        ' --- Define Non-Ecom DB tables here ---

        Public Property Minyanims As DbSet(Of Ecom.Minyanim)
        Public Property ShulInfos As DbSet(Of Ecom.ShulInfo)

        Protected Overrides Sub OnModelCreating(modelBuilder As System.Data.Entity.DbModelBuilder)
            MyBase.OnModelCreating(modelBuilder)

            ' --- Add the mappings here ---
            modelBuilder.Configurations.Add(New Ecom.MinyanimMap)
            modelBuilder.Configurations.Add(New Ecom.ShulInfoMap)

            Entity.Database.SetInitializer(Of BlueBasisDbContext)(Nothing)

            MapEcomObjects(modelBuilder)
            MapCustomObjects(modelBuilder)
        End Sub

        Partial Private Sub MapEcomObjects(modelBuilder As DbModelBuilder)
        End Sub

        Partial Private Sub MapCustomObjects(modelBuilder As DbModelBuilder)
        End Sub


    End Class

End Namespace

