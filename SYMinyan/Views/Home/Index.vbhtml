﻿@ModelType SYMinyan.MinyanimModels

@Code
    ViewData("Title") = "Home Page"
End Code

@Section Head
<style>

    
</style>
<script src="http://www.paskie.com/zman/default.asp?webserve=3&city=10310"></script>
<script>
    $(function () {
        var zmans = '';
        $.each(zmanx, function (k, v) {
            //display the key and value pair
            if (k != 'succ') {
                zmans += ('<b>' + k + ':</b> <span>' + v + '</span><br />');
            };
        });
        $("#zmanim").html(zmans);
    });
</script>
End Section

        <div class="container">
            <div class="row header">
                <div class="col-md-3 col-sm-12 big-button">
                    <div>
                    <select class="shul-dropdown" onChange="window.location.href='@Url.Action("Index", "Shuls")'+'/Index/'+this.value+'#results'">
                    <option valyue="">By Shul / Location</option>
                    @For Each shul In Model.ShulListing

                        @<option value="@shul.Shul">@shul.Shul</option>
  
                    Next
                    </select>
                    </div>
                </div>
                <div class="clearfix col-md-1 hidden-sm-down"></div>
                <div class="col-md-2 col-sm-12">
                    <a class="navlinks" href="#remain">remaining minyanim</a>
                </div>
                <div class="col-md-2 col-sm-12">
                    <a class="navlinks" href="#manana">minyanim tomorrow</a>
                </div>
                 <div class="col-md-2 col-sm-12">
                    <a class="navlinks" href="#news"><img src="@Url.Content("~/images/icon_exclaim.png")" class="fix-icon" />important updates</a>
                </div>
                <div class="col-md-2 col-sm-12">
                    <a class="navlinks" href="mailto:alex@blueswitch.com"><img src="@Url.Content("~/images/icon_plus.png")"  class="fix-icon" />add your shul</a>
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <div class="line"></div>
                </div>
            </div>
        </div>


        <div class="container "   style="margin-top:20px;">
            <div class="row buffertop">
                <div class="col-md-3  col-sm-12">
                        <div class="padding50 hideonmobile"></div>
                        @code
                            Dim TimeCols As String = "#d5d6d7,#bac8da,#95b3d7,#6799d7,#4a8ad7,#4a8ad7"
                            Dim TimeColX = TimeCols.Split(",")
                            Dim currcol As String
                        Dim numtimes As Integer = 0
                            Dim fsize = 11
                            Dim timecol As String = ""
                           For Each blah In Model.PreviousThree
                                Dim ttime As DateTime = DateTime.ParseExact(blah.MTime.ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
                                Dim ttime2 = ttime.ToString("h:mm tt", System.Globalization.CultureInfo.InvariantCulture)
                                If ttime2 = "4:30 AM" Then ttime2 = "Netz"
                                If numtimes < 5 Then
                                    currcol = TimeColX(numtimes)
                                Else
                                    currcol = TimeColX(4)
                                End If
                            @<text>
                            <span style="line-height:36px; font-size:16px; color: @currcol">@ttime2 - @blah.MLocation</span><br />
                            </text>
                            fsize = fsize + 2
                            numtimes = numtimes + 1
                            If numtimes = 5 Then Exit For
                        Next
                        fsize = fsize+2
                        End Code

                </div>
                
                <div class="col-md-1 hidden-sm-down">
                    <img src="@Url.Content("~/images/bigarrow.png")" />
                </div>

                <div class="col-md-4  col-sm-12 vcenter ">
                        <div class="padding100 hideonmobile"></div>
                        <div style="width:290px; margin:0 auto 0 auto;">
                         <span style="font-size:50px;">@FormatDateTime(System.DateTime.Now, DateFormat.LongTime)</span><br />
                         <span style="float:right; font-size:24px; color:#666; ">time now</span>
                         </div>
                </div>

                <div class="col-md-1 hidden-sm-down"  style="min-width:70px;">
                    <img src="@Url.Content("~/images/bigarrow.png")" />
                </div>

                <div class="col-md-2 col-sm-12 vcenter">
                    @code
                        Dim TimeColZ = TimeColX.Reverse
                        
                        numtimes = 0
                       For Each blah In Model.NextThree
                            Dim ttime As DateTime = DateTime.ParseExact(blah.MTime.ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
                            
                            Dim ttime2 = ttime.ToString("h:mm tt", System.Globalization.CultureInfo.InvariantCulture)
                            If numtimes < 6 Then
                                currcol = TimeColZ(numtimes)
                            Else
                                currcol = TimeColZ(5)
                            End If
                            If (Weekday(Now) = 6 And ttime2.Contains("AM")) Or Weekday(Now) < 6 Then
                        @<text>
                        <span style="line-height:36px; font-size:16px; color: @currcol">@ttime.ToString("h:mm tt",System.Globalization.CultureInfo.InvariantCulture) - @blah.MLocation</span><br />
                        </text>
                            End If
                            numtimes = numtimes + 1
                            fsize = fsize - 2
                            If numtimes = 8 Then Exit For
                        Next
                    End Code
                </div>

            </div>
        </div>


        <div class="container ">
            <div class="row">
                <div class="col-md-8 paddit">
                
                @Html.Partial("~\Views\Shared\_Gmaps.vbhtml", Model)
                
                </div>
            
                
                <div class="col-md-4 paddit ">
                    <div style="width:90%; float:left; background-color:#ebebeb; margin:0 0 0 20px;">
                        <div style="margin:20px 20px 20px 30px">
                            zemanim times for deal area<br /><br />
                            <div id="zmanim"  style="font-size:12px; line-height:18px; color: #767676;"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="container" >
            <div class="row">
                <div class="col-md-6 col-sm-12 paddit" >

                    <div class="container">
                        <div class="row" style="background-color:#e5f1ff">
                            <div class="col-md-6 col-sm-12">
                                <div style="margin:20px 0 0 20px;">
                                     <a name="remain"></a>
                                    <span style="font-size:20px;">remaining minyanim</span>
                                    <br /><span style="font-size:28px;">today</span>
                                  </div>
                            </div>
                            <div class="col-md-6 col-sm-12" style="color:#666;">
                                <div style="margin:20px;">
                                     @code
                                         numtimes = 0
                                         Dim shabbatmsg = False
                                        For Each blah In Model.NextThree
                                            Dim ttime As DateTime = DateTime.ParseExact(blah.MTime.ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
                                            Dim ttime2 = ttime.ToString("h:mm tt", System.Globalization.CultureInfo.InvariantCulture)
                                             If ttime2 = "4:30 AM" Then ttime2 = "Netz "
                                             If (Weekday(Now) = 6 And ttime2.Contains("AM")) Or Weekday(Now) < 6 Then
                                        @<text>
                                        <span style="font-size:12px;">@ttime2 - @blah.MLocation</span><br />
                                        </text>
                                             ElseIf Not shabbatmsg Then
                                                 @<span>Please check shuls for Shabbat schedule. </span>
                                                 shabbatmsg = True
                                             End If
                                        numtimes = numtimes + 1
                                            If numtimes = 10 Then
                                                @<text>
                                                <a id="morelink" href="javascript:$('#more').show();$('#morelink').hide();">(... more)</a>
                                                <div id="more" style="display:none;">
                                                </text>
                                               
                                            End If
                                    
                                         Next
                                         If numtimes > 10 Then
                                             @:</div>
                                         End If
                                    End Code
                                </div>
                            </div>
                        </div>
                   </div>
            </div>
            <div class="col-md-6 col-sm-12 paddit" >
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                               <div style="margin:20px 0 0 20px;">
                                <a name="manana"></a>
                                <span style="font-size:20px;">upcoming minyanim </span>
                                <br /><span style="font-size:28px;">tomorrow</span>
                              </div>

                            </div>
                            <div class="col-md-6 col-sm-12" style=" color:#666;">
                               <div style="margin:20px;">
                                       @code
                                        numtimes = 0
                                        For Each blah In Model.Minyanlist
                                            Dim ttime As DateTime = DateTime.ParseExact(blah.MTime.ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
                                            Dim ttime2 = ttime.ToString("h:mm tt", System.Globalization.CultureInfo.InvariantCulture)
                                            If ttime2 = "4:30 AM" Then ttime2 = "Netz "
                                        @<text>
                                        <span style="font-size:12px;">@ttime2 - @blah.MLocation</span><br />
                                        </text>
                                        numtimes = numtimes + 1
                                            If numtimes = 10 Then
                                                        @<a href="@Url.Action("Index","Shuls")">(... more)</a>
                                                Exit For
                                            End If
                           
                                    Next 
                                    End Code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <div class="container" >
            <div class="row paddit" style="">
                <div class="col-md-4 col-sm-12 " style="background-color:#3b7bca;" >
                    <div style="width:100%;  margin: 30px; color:#fff; font-size:20px;">
                    <img src="@Url.Content("~/images/bigimport.png")" /><br />
                    important updates
                    </div>
                </div>

                 <div class="col-md-8 col-sm-12" style="background-color:#3b7bca;"  >
                    <div style="width:100%;  margin: 30px; color:#fff">
                    Nothing to report.
                    </div>
                </div>
                
            </div>
        </div>


        <div class="container" >
            <div class="row justify-content-center">

                <div class="col-md-2 col-sm-12 paddit" >
                   Leiluy Nishmat<br />
                   <B>Eddie Paskie</B><br />
                   Ezra Refael Ben Mazal
                </div>
                <div class="col-md-2 col-sm-12 paddit" >
                   Leiluy Nishmat
                   <br />
                   <B>Elliot Chalme</B><br />
                   Eliyahu Ben Miriam
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-2 col-sm-12 paddit" >
                Leiluy Nishmat<br />
                Abraham Ben Rahel
                </div>
                <div class="col-md-2 col-sm-12 paddit" >
                Leiluy Nishmat<br />
                Abraham Ben Sophie
                </div>
                <div class="col-md-2 col-sm-12 paddit" >
                    Refuah Shelemah<br />
                    Eliezer Ben Jamileh
                </div>
                <div class="col-md-2 col-sm-12 paddit" >
                    Refuah Shelemah<br />
                    Hana Bat Pessel
                </div>
            </div>

            <div class="row">
            
                <div class="col-md-2 col-sm-12 paddit" >
                Refuah Shelemah<br />
                    David Ben Yaffa
                </div>
                <div class="col-md-2 col-sm-12 paddit" >
                Refuah Shelemah<br />
                Refael Ben Hana
                </div>
                <div class="col-md-2 col-sm-12 paddit" >
                    Refuah Shelemah<br />
                     Aharon Ben Celia Simha
                </div>
                <div class="col-md-2 col-sm-12 paddit" >
                    Refuah Shelemah<br />
                    Shaul Ben Dinah
                </div>            
                <div class="col-md-2 col-sm-12 paddit" >
                    Refuah Shelemah<br />
                    Rina Haya Bat Leah
                </div>  
                <div class="col-md-2 col-sm-12 paddit" >
                    Refuah Shelemah<br />
                    Elisheba bat Esther
                </div>                              
            </div>



<span style="font-size:12px;">Sponsored By:</span> <a href="http://www.blueswitch.com" style="color:#666;font-size:12px; text-decoration:none;">BlueSwitch</a>


