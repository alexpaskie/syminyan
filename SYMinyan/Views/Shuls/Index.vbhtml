﻿@ModelType SYMinyan.MinyanimModels

@Code
    ViewData("Title") = "Shuls"
End Code



     <div class="container">
            <div class="row header">
                <div class="col-md-3 col-sm-12 big-button">
                    <div>
                    <select  class="shul-dropdown"  onChange="window.location.href='@Url.Action("Index", "Shuls", New With {.Id = Nothing})'+'/Index/'+this.value+'#results'">
                    <option value="">By Shul / Location</option>
                    @For Each shul In Model.ShulListing

                        @<option value="@shul.Shul" @IIf(shul.Shul = Model.CurrentShul, "SELECTED", "")>@shul.Shul</option>
  
                    Next
                    </select>
                    </div>
                </div>
                <div class="clearfix col-md-1 hidden-sm-down"></div>
                <div class="col-md-2 col-sm-12">
                    <a class="navlinks" href="@Url.Action("Index","Home")">home</a>
                </div>
                <div class="col-md-2 hidden-sm-down">             
                </div>
                 <div class="col-md-2 hidden-sm-down">
                   
                </div>
                <div class="col-md-2 col-sm-12">
                    <a class="navlinks" href="mailto:alex@blueswitch.com"><img src="@Url.Content("~/images/icon_plus.png")"  class="fix-icon" />add your shul</a>
                </div>

            </div>
            <div class="row buffertop">
                <div class="col">
                    <div class="line"></div>
                </div>
            </div>
        </div>


 
<a name="results"></a>

<div class="container">


@If Model.Minyanlist IsNot Nothing Then
    
    
    @<div class="row buffertop" style="margin-top:20px;">
       <div class="col-md-12 buffertop" style="color:#666;">
            <strong>@Model.ShulName.RealName</strong> 
                <br />
                <a href="http://maps.google.com?q=@Model.ShulName.Address" style="text-decoration:underline;">@Model.ShulName.Address</a>
                <br /><br />    
       </div>
    </div>

    
    @<div class="row">
        @For eachday = 0 To 5
            Dim flagday = False
            @<div class="col-md-2 col-sm-6 col-xs-6 time-table-thin">
            <div class="buffertop">
            <span style="font-size:18px">@WeekdayName(eachday+1)</span><br />
            @For Each minyan In Model.Minyanlist
                    If minyan.MDay = eachday Then
                        Dim ttime As DateTime = DateTime.ParseExact(minyan.MTime.ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
                        Dim ttime2 = ttime.ToString("h:mm tt", System.Globalization.CultureInfo.InvariantCulture)
                        If (minyan.MDay = 5 And ttime2.Contains("AM")) Or minyan.MDay < 5 Then
                            @<span style="color:#666;">@IIf(ttime2 = "4:30 AM","Netz",ttime2)</span>
                            @<br />
            End If
                    
            End If
                
        Next

                </div>
            </div>
            Next
            
        </div>
    

Else
   @<div class="container">
            <div class="row header">
                <div class="col-md-12">
                Please choose a shul from the dropdown above to see its complete schedule.
                </div>
            </div>
    </div>
    
End If

</div>
<br /><br />
@Html.Partial("~\Views\Shared\_Gmaps.vbhtml", Model)
