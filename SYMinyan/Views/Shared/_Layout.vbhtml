﻿<!DOCTYPE html>
<html lang="en">
    <head>
        
        @Scripts.Render("~/bundles/jquery")
        <!--link rel="stylesheet" type="text/css" href="@Url.Content("~/css/simplegrid.css")" /-->
        <link rel="stylesheet" type="text/css" href="@Url.Content("~/css/bootstrap.min.css")" />
        <link rel="stylesheet" type="text/css" href="@Url.Content("~/css/style.css")" />
        <link rel="stylesheet" type="text/css" href="@Url.Content("~/css/bootstrap-grid.min.css")" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <style>
        body, td, div, span, a {  text-decoration:none; }
        
        #map { width:100%; height:300px; }
        </style>
        @RenderSection("head", required:=False)
    </head>
    <body style="background-color:#ffffff; color:#4989d7; font-family:Arial, Verdana; margin:0; padding:0">


    <div class="container">
			<div class="row">
				<div class="col" >



                @RenderBody()
         
    
    
    
    
            <br />
        <a href="mailto:alex@blueswitch.com">Contact</a>


				</div>
			</div>
		</div>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-360859-2', 'auto');
            ga('send', 'pageview');

</script>

    
    </body>
</html>
