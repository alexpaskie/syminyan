﻿@ModelType SYMinyan.MinyanimModels

 <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content" style="font-size:12px;">
                <div id="map"></div>
            </div>
        </div>
    </div>
<script>

    
        var icon= "@url.Content("~/images/jstarmed.png")";
        var ulocation = { lat: 40.265099, lng: -74.000610};
        var map;

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } 
            else {
                markCurrentloc();
            }
        };

        function showPosition(position) {
                ulocation = { lat: position.coords.latitude, lng: position.coords.longitude }; 
                markCurrentloc();
            }





    function initMap() {
        getLocation()
        var myLatLng = ulocation;
 
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: ulocation
        });


            @code
                Dim marknum = 0
                Dim currshulloc = ""
            End Code

            @For Each shul In Model.ShulListing
                marknum = marknum + 1
                If shul.Shul = Model.CurrentShul Then
                    currshulloc = "lat: " & shul.Latitude & ", lng: " & shul.Longitude
                End If
            @<text>
            var marker_@marknum = new google.maps.Marker({
            position: { lat: @shul.Latitude, lng: @shul.Longitude },
            map: map,
            title: '@shul.RealName',
            icon: icon
            });  

            var tmark = marker_@marknum;
            tmark.addListener('click', function() {
                window.location.href='@Url.Action("Index", "Shuls", New With {.id = shul.Shul})';
                })

            </text>
            
        Next

        
        @IIf(currshulloc <> "", "markCurrentloc();", "")

        @IIf(currshulloc <> "", "map.center({ " & currshulloc & "});", "")
    }



        function markCurrentloc() {
        console.log('entre - inside marklock');
         var marker = new google.maps.Marker({
         position: { @IIf(currshulloc<>"",currshulloc,"ulocation") },
         map:         map,
        title: 'Hello World!'
         });  
         map.setCenter(ulocation); 
    }

    $(function() {
        getLocation();
    });

    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtDE1xPCWwtzYZ7mfoFXOlQEiKV_zH53E&callback=initMap"></script>