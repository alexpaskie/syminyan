﻿Imports SYMinyan.BlueBasis
Imports SYMinyan.BlueBasis.Ecom
Imports System.Globalization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim Data As New MinyanimModels
        Dim Minyanlist = Establish.Db.Minyanims.ToList
        'Dim dtTemp As DateTime = DateTime.ParseExact(Span.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture)
        Dim nowtime = System.DateTime.Now.ToString("HH:mm:ss")
        Dim nowtimep = TimeSpan.Parse(nowtime)
        Dim noontime = TimeSpan.Parse("12:00:00")
        Dim todaysd = Weekday(System.DateTime.Now) - 1
        Data.PreviousThree = (From p In Minyanlist Select p Where (p.MDay = todaysd And p.MTime <= nowtimep)).OrderByDescending(Function(m) m.MTime).ThenBy(Function(p) p.MLocation).ToList
        Data.NextThree = (From p In Minyanlist Select p Where (p.MDay = todaysd And p.MTime > nowtimep)).OrderBy(Function(m) m.MTime).ToList
        Dim tx = (From p In Data.PreviousThree Select p Take 5).OrderBy(Function(m) m.MTime).ToList
        Data.PreviousThree = tx
        Dim manana = todaysd + 1
        If manana = 7 Then manana = 0
        Data.Minyanlist = (From p In Minyanlist Select p Where (p.MDay = manana And p.MTime < noontime)).OrderBy(Function(m) m.MTime).ToList
        Data.ShulListing = Establish.Db.ShulInfos.OrderBy(Function(p) p.Shul).ToList



        Return View(Data)
    End Function

    Function About() As ActionResult
        ViewData("Message") = "Your app description page."

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "Your contact page."

        Return View()
    End Function
End Class
