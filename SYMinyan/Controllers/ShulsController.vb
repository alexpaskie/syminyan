﻿Imports SYMinyan.BlueBasis

Namespace SYMinyan
    Public Class ShulsController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Shuls

        Function Index(Optional ByVal id As String = Nothing) As ActionResult
            Dim Minyanlist = (From p In Establish.Db.Minyanims Select p.MLocation Order By MLocation).Distinct.ToList

            Dim Data As New MinyanimModels
            Data.ShulListing = Establish.Db.ShulInfos.OrderBy(Function(p) p.Shul).ToList
            Data.CurrentShul = ""

            Dim shulSelected As Ecom.ShulInfo

            If id IsNot Nothing Then
                Data.Minyanlist = (From p In Establish.Db.Minyanims Select p Where (p.MLocation = id)).OrderBy(Function(l) l.MDay).ThenBy(Function(m) m.MTime).ToList

                Dim tx = Establish.Db.ShulInfos.Where(Function(p) p.Shul = id).ToList
                If tx.Count > 0 Then
                    shulSelected = tx(0)

                End If
                Data.ShulName = shulSelected
                Data.CurrentShul = id
            End If



            Return View(Data)
        End Function



        '
        ' GET: /Shuls/Details/5

        Function Details(ByVal id As Integer) As ActionResult

        End Function

        '
        ' GET: /Shuls/Create

        Function Create() As ActionResult
            Return View()
        End Function

        '
        ' POST: /Shuls/Create

        <HttpPost()> _
        Function Create(ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add insert logic here
                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function

        '
        ' GET: /Shuls/Edit/5

        Function Edit(ByVal id As Integer) As ActionResult
            Return View()
        End Function

        '
        ' POST: /Shuls/Edit/5

        <HttpPost()> _
        Function Edit(ByVal id As Integer, ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add update logic here

                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function

        '
        ' GET: /Shuls/Delete/5

        Function Delete(ByVal id As Integer) As ActionResult
            Return View()
        End Function

        '
        ' POST: /Shuls/Delete/5

        <HttpPost()> _
        Function Delete(ByVal id As Integer, ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add delete logic here

                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function
    End Class
End Namespace