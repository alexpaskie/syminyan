﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Entity.ModelConfiguration

Namespace BlueBasis.Ecom

#Region "Database Map"
    Partial Public Class ShulInfoMap
        Inherits BaseShulInfoMap

    End Class

    Public Class BaseShulInfoMap
        Inherits EntityTypeConfiguration(Of ShulInfo)

        Sub New()
            MyBase.New()

            HasKey(Function(m) m.RecID)


            [Property](Function(m) m.RecID).HasColumnName("rec_id")
            [Property](Function(m) m.Shul).HasColumnName("shul")
            [Property](Function(m) m.Address).HasColumnName("address")
            [Property](Function(m) m.RealName).HasColumnName("real_name")
            [Property](Function(m) m.RealName).HasColumnName("latitude")
            [Property](Function(m) m.RealName).HasColumnName("real_name")
            ToTable("shuls")
        End Sub

    End Class

#End Region

    Partial Public Class ShulInfo
        Inherits BaseShulInfo

    End Class

    Public Class BaseShulInfo

        Public Property RecID As Integer
        Public Property Shul As String
        Public Property Address As String
        Public Property RealName As String
        Public Property Latitude As String
        Public Property Longitude As String
    End Class

End Namespace
